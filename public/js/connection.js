function checkUser(email, password) {
    for (let i in window.users) {
        var user = window.users[i];
        if (user.email === email && user.password === password)
            return true;
    }

    return false;
}

function getUsername(email) {
    return "./user/" + email.split('@')[0] + "/";
}

async function connect(form) {
    var email = form.querySelector("#email").value;
    var password = form.querySelector("#password").value;

    if (email === "admin@osm.evil" && password === "0SmAdm1Ndp")
        form.action = "./admin/";
    else if (checkUser(email, password))
        form.action = getUsername(email);
    else
        form.action = "./401/";
}
