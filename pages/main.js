(async function main() {
    'use strict';

    const response = await fetch("/js/users.json");
    const content = await response.json();
    window.users = content.users;
})();
