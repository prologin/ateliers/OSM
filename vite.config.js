import { resolve } from 'path'

module.exports = {
    base: "/",
    appType: "mpa",
    root: "pages",
    publicDir: "../public",
    build: {
        minify: false,
        rollupOptions: {
            input: {
                main: resolve(__dirname, "pages/index.html"),
                admin: resolve(__dirname, "pages/admin/index.html"),
                401: resolve(__dirname, "pages/401/index.html"),
                mfontaine: resolve(__dirname, "pages/user/mfontaine/index.html"),
            }
        },
        outDir: "../dist"
    }
}
