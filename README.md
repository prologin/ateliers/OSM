# OSM (Organisation Secrète Maléfique)

Ce repo est le code source du site de l'entreprise maléfique de l'atelier 'Agent Compromis'.

Pour entrer sur la page administrateur, les participants doivent regarder dans
le fichier js de la page. Le mot de passe y est écrit avec un simple `if`.

Cette page est également utilisée pour se connecter au compte de la taupe, et de
recueillir les preuves finales pour son inculpation.
